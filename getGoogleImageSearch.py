# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import re


def cprint(color_num, mssg):
    print("\033[%sm%s\033[0m" % (color_num, mssg))



def get_target_url(filename):
    postUrl = 'http://www.google.co.jp/searchbyimage/upload'
    files = {'encoded_image': open(filename, "rb")}
    res = requests.post(postUrl, files=files)

    try:
        bsObj = BeautifulSoup(res.text, "html.parser")
        print("Successfly make beautiful soup!")
    except AttributeError as e:
        print("Error 01: %s" % e)
        quit()

    regxp = re.compile("(https://accounts.google.com/).*(sbi:).*")

    for atag in bsObj.findAll("a"):
        url = atag.attrs["href"]
        if regxp.match(url):
            sbicode = url.split("sbi:")[1]
            target_url = "https://www.google.co.jp/search?tbs=sbi:" + sbicode
            cprint(35, target_url)

    return target_url

if __name__ == '__main__':
    target_url = get_target_url('test.png')
    r = requests.get(target_url)
    # test
    # print(r.text)
    try:
        bsObj = BeautifulSoup(r.text, "html.parser")
    except AttributeError as e:
        print("Error 02: %s" % e)
        quit()
    print(bsObj.get_text())
        
