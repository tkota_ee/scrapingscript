#!/usr/bin/env/python
#coding: utf-8

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests, sys, json

# Grobal contents
baseUrl = "https://www.google.co.jp/search?&num=1&ie=UTF-8&"
headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5)AppleWebKit 537.36 (KHTML, like Gecko) Chrome", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}


def get(searchWord):
    session = requests.Session()
    url = baseUrl + "&q=\'%s\'" % searchWord
#    url = "https://www.google.co.jp/search?espv=2&q=yahoo.co.jp&oq=yahoo.co.jp&gs_l=serp.3..0l10.2984005.2985787.0.2985986.12.10.0.2.2.0.92.770.10.10.0....0...1c.1.64.serp..0.11.701.0..0i131k1j0i4k1j0i131i4k1.yZnJ3eZ3pvM"
    cprint(35, url)
    try:
        req = session.get(url, headers=headers)
        cprint(0, "Successfly session.get!")
    except ConnectionError as e:
        cprint(31, "Error 01: %s" % e)
        get(url) #try again
    try:
        bsObj = BeautifulSoup(req.text, "html.parser")
        cprint(0, "Successfly make beautiful soup!\n")
    except AttributeError as e:
        cprint(31, "Error 02: %s" % e)
        return None

    title = getTitle(bsObj)
    summary = getSummary(bsObj)
    cprint(36, title)
    cprint(32, url)
    cprint(37, summary)
#    cprint(0, json.dumps({"title":title, "summary":summary}, indent=4))


def getTitle(bsObj):
#    print(bsObj.findAll("div", {"id":"search"}))
#    print(len(bsObj.findAll("h3", {"class":"r"})))
#    print(bsObj.findAll("h3", {"class":"r"})[0].find("a").text)
    return bsObj.findAll("h3", {"class":"r"})[0].find("a").text

def getSummary(bsObj):
#    print(bsObj.findAll("span", {"class":"st"}))
    return bsObj.findAll("span", {"class":"st"})[0].text


def cprint(color_num, mssg):
    print('\033[%sm%s\033[0m' % (color_num, mssg))


def sys_arg(args):
    if len(args) != 2:
        help_mssg = " Usage: python %s [search text]" % args[0]
        cprint(36, help_mssg)
        quit(1)
    else:
        return args[1]


if __name__ == '__main__':
    searchWord = sys_arg(sys.argv)

    get(searchWord)
