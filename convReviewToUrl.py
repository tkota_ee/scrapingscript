#!/usr/bin/env/python
#coding: utf-8

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests, json, csv, re

# Grobal contents
FEATURES_LIST = "./features.list"
HEADERS = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5)AppleWebKit 537.36 (KHTML, like Gecko) Chrome", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}


def get(url):
    cprint(35, url)
    session = requests.Session()
    try:
        req = session.get(url, headers=HEADERS)
        cprint(0, "Successfly session.get!")
    except ConnectionError as e:
        cprint(31, "Error 01: %s" % e)
        get(url) #try again
    try:
        bsObj = BeautifulSoup(req.text, "html.parser")
        cprint(0, "Successfly make beautiful soup!")
    except AttributeError as e:
        cprint(31, "Error 02: %s" % e)
        return None

    title = getTitle(bsObj)
    description = getDescription(bsObj)
    return {"url":url, "title":title, "summary":description}
    

def getTitle(bsObj):
    try:
        return bsObj.findAll("title")[0].text
    except:
        return "no-title"

def getDescription(bsObj):
    try:
        return bsObj.findAll("meta", {"name":"description"})[0].attrs["content"]
    except:
        return "no-description"


def cprint(color_num, mssg):
    print('\033[%sm%s\033[0m' % (color_num, mssg))


def cleanseJson(strJson):
    strJson = re.sub(",[ \t\r\n]*\]", "]", strJson)
    strJson = re.sub(",[ \t\r\n]*}", "}", strJson)
    return strJson

def updateTags(tags):
    updateTags = []
    for dctTag in dctTagInfo["tag"]:
        if "review" in dctTag.keys() and regexp.match(dctTag["review"]):
            updateTags.append(get(dctTag["review"]))
        else:
            updateTags.append(dctTag)
    return updateTags


if __name__ == '__main__':
    regexp = re.compile("^http[s]*:\/\/")

    #read
    reader = csv.reader(open(FEATURES_LIST, 'r'), delimiter='\t')
    featuresList = [row for row in reader]
    
    #update
    for i in range(len(featuresList)):
        print("----")
        cprint(36, featuresList[i][2])
        #parse dctTagInfo from featuresList[i]
        dctTagInfo = json.loads(cleanseJson(featuresList[i][2]))
        #update dctTagInfo
        tags = dctTagInfo["tag"]
        dctTagInfo["tag"] = updateTags(tags)
        #update featuresList[i]
        featuresList[i][2] = json.dumps(dctTagInfo, ensure_ascii=False).replace (" ", "")
        cprint(36, featuresList[i][2])

    #write
    f = open(FEATURES_LIST+".update", 'w')
    for l in featuresList:
        f.writelines(l[0]+"\t"+l[1]+"\t"+l[2]+"\n")
    f.close()

