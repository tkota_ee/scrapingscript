#!/usr/bin/env/python
#coding: utf-8

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests, json, csv, re

# Grobal contents
FEATURES_LIST = "./features.list"


def cprint(color_num, mssg):
    print('\033[%sm%s\033[0m' % (color_num, mssg))

def cleanseJson(strJson):
    strJson = re.sub(",[ \t\r\n]*\]", "]", strJson)
    strJson = re.sub(",[ \t\r\n]*}", "}", strJson)
    return strJson

def updateTags(tags):
    updateTags = []
    for dctTag in dctTagInfo["tag"]:
        if "review" in dctTag.keys() and regexp.match(dctTag["review"]):
            updateTags.append(get(dctTag["review"]))
        else:
            updateTags.append(dctTag)
    return updateTags


if __name__ == '__main__':
    regexp = re.compile("^http[s]*:\/\/")

    #read
    reader = csv.reader(open(FEATURES_LIST, 'r'), delimiter='\t')
    featuresList = [row for row in reader]
    
    #update
    for i in range(len(featuresList)):
        print("----")
        cprint(36, featuresList[i][2])
        #parse dctTagInfo from featuresList[i]
        dctTagInfo = json.loads(cleanseJson(featuresList[i][2]))
        #update dctTagInfo
        updateTags = []
        for tag in dctTagInfo["tag"]:
            if "url" in tag.keys():
                url = tag["url"]
                updateTags.append({"review":url})
            else:
                print(tag)
                updateTags.append(tag)

        dctTagInfo["tag"] = updateTags
        #update featuresList[i]
        featuresList[i][2] = json.dumps(dctTagInfo, ensure_ascii=False).replace (" ", "")
        cprint(35, featuresList[i][2])

    #write
    f = open(FEATURES_LIST+".update", 'w')
    for l in featuresList:
        f.writelines(l[0]+"\t"+l[1]+"\t"+l[2]+"\n")
    f.close()

