#!/usr/bin/env/python
#coding: utf-8

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests, sys, json

# Grobal contents
headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5)AppleWebKit 537.36 (KHTML, like Gecko) Chrome", "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}


def get(url):
    cprint(35, url)
    session = requests.Session()
    try:
        req = session.get(url, headers=headers)
        cprint(0, "Successfly session.get!")
    except ConnectionError as e:
        cprint(31, "Error 01: %s" % e)
        get(url) #try again
    try:
        bsObj = BeautifulSoup(req.text, "html.parser")
        cprint(0, "Successfly make beautiful soup!\n")
    except AttributeError as e:
        cprint(31, "Error 02: %s" % e)
        return None

    title = getTitle(bsObj)
    description = getDescription(bsObj)
    cprint(36, title)
    cprint(32, url)
    cprint(37, description)
    

def getTitle(bsObj):
#    print("len:",len(bsObj.findAll("title")))
#    for titleObj in bsObj.findAll("title"):
#        cprint(36, titleObj.text)
    try:
        return bsObj.findAll("title")[0].text
    except:
        return "no-title"

def getDescription(bsObj):
#    print("len:",len(bsObj.findAll("meta", {"name":"description"})))
#    for metaObj in bsObj.findAll("meta", {"name":"description"}):
#        cprint(36, metaObj.attrs["content"])
    try:
        return bsObj.findAll("meta", {"name":"description"})[0].attrs["content"]
    except:
        return "no-description"


def cprint(color_num, mssg):
    print('\033[%sm%s\033[0m' % (color_num, mssg))


def sys_arg(args):
    if len(args) != 2:
        help_mssg = " Usage: python %s [url]" % args[0]
        cprint(36, help_mssg)
        quit(1)
    else:
        return args[1]


if __name__ == '__main__':
    url = sys_arg(sys.argv)

    get(url)
